#!/bin/bash
# purge ou recyclage des dossiers groupes/donnes et travail
# 06/2018 by JLD
rouge='\e[0;31m'
orange='\e[0;33m'
jaune='\e[1;33m'
bleuclair='\e[1;34m'
normal='\e[0;m'
annee=$(date +%Y)


#pwd
messbleu(){
echo -e "${bleuclair} $1 ${normal}"
}
messjaune(){
echo -e "${jaune} $1 ${normal}"
}
messorange(){
echo -e "${orange} $1 ${normal}"
}
messrouge(){
echo -e "${rouge} $1 ${normal}"
}
oui_non(){
 #fonction oui/non
 if echo "$1" |grep -q -E -i '^o(ui)?$' ; then 
 	return 0
 else
	return 1
 fi
}
supprime(){

messrouge "Attention tous les fichiers vont être supprimés"
messrouge "voulez vous executer le script ? O/n"
read -rp "O ou n ? : " REPLY
#if [ "$REPLY" = O ] || [ "$REPLY" = o ] || [ "$REPLY" = oui ]; then
if oui_non "$REPLY"; then
   ls -1 | while read -r i
   do
           if [ -d "$i" ]
           then
               if [ "$i" == "commun" ] || [ "$i" == "devoirs" ]
               then
                   messrouge "Pas de traitement de $i"
               else
                   if [ -d "/home/workgroups/$i/donnees" ]
                   then
                       messbleu "purge de $i/donnees"
                       rm -rf /home/workgroups/"$i"/donnees/*
                   fi
                   if [ -d "/home/workgroups/$i/travail" ]
                   then
                       messbleu "purge de $i/travail"
                       rm -rf /home/workgroups/"$i"/travail/*
                   fi
   
               fi
           fi
   done
fi
}
deplace(){
	
	if [ -d "/home/recyclage/$annee/groupes/" ]
	then 
		messorange "Le repertoire groupes existe"
    else
        messbleu "Creation du repertoire de destination"
        mkdir -p /home/recyclage/"$annee"/groupes
    fi
    
    ls -1 | while read -r i
do
        if [ -d "$i" ]
        then
            if [ "$i" == "commun" ] || [ "$i" == "devoirs" ]
            then
				echo -e "${rouge}"
                echo "Pas de traitement de $i "
                echo -e "${normal}"
            else
                if [ -d "/home/workgroups/$i/donnees" ]
                then
					mkdir -p /home/recyclage/"$annee"/groupes/"$i"/{donnees,travail}
                    messbleu "recyclage de $i/donnees"
                    mv /home/workgroups/"$i"/donnees/* /home/recyclage/"$annee"/groupes/"$i"/donnees/
                    messbleu "recyclage de $i/travail"
                    mv /home/workgroups/"$i"/travail/* /home/recyclage/"$annee"/groupes/"$i"/travail/
                    chown -R root:root /home/recyclage/"$annee"/groupes/"$i"
                fi

            fi
        fi
    
done
	

}
cd /home/workgroups || exit 
case $1 in 
	s ) supprime ;;
	d ) deplace ;;
	* ) 
	messjaune "Traitement des dossiers données et travail"
	messrouge "s : Suppression de TOUT le contenu des dossiers données/travail"
	messbleu "d : Déplacements des dossiers données/travail vers recyclage (/home/recyclage/$annee/groupes/)"

esac
